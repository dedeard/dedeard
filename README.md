<h1 align="center">Hi 👋, I'm Dede ariansya</h1>
<h3 align="center">A passionate web developer from Makassar, Indonesia</h3>

<p align="left"> <img src="https://komarev.com/ghpvc/?username=dedeardiansya&label=Profile%20views&color=0e75b6&style=flat" alt="dedeardiansya" /> </p>

- 👨‍💻 All of my projects are available at [https://www.dedeard.my.id](https://www.dedeard.my.id)

- 📫 How to reach me **me@dedeard.my.id**

- ⚡ Fun fact **I think I am funny**

<p><img align="center" src="https://github-readme-stats.vercel.app/api/top-langs?username=dedeardiansya&show_icons=true&locale=en&layout=compact" alt="dedeardiansya" /></p>
